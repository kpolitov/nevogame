#ifndef TOOLS_H
#define TOOLS_H

struct Rect
{
    float left;
    float top;
    float right;
    float bottom;
    Rect(int l, int t, int r, int b): left(l), right(r), top(t), bottom(b) {}
    Rect(): left(0), right(0), top(0), bottom(0) {}
    Rect& operator=(const Rect& rect)
    {
        left = rect.left;
        top = rect.top;
        right = rect.right;
        bottom = rect.bottom;
    }
};

/* Requirements restriction */

class Renderer
{
public:
    virtual void Render(const Rect& screenCoords, int textureId, const Rect& textureCoord) = 0;
};

extern Renderer* currentRenderer;

#endif // TOOLS_H
