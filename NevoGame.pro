#-------------------------------------------------
#
# Project created by QtCreator 2013-10-03T00:50:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NevoGame
TEMPLATE = app

CONFIG += static

SOURCES += main.cpp \
    MainWindow.cpp \
    Tools.cpp \
    NevoGame.cpp

HEADERS  += \
    MainWindow.h \
    NevoGame.h \
    Tools.h

FORMS    += \
    MainWindow.ui
