#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QMouseEvent>
#include <QPainter>
#include <QtConcurrent/QtConcurrentRun>
#include <QtCore>
#include <unistd.h>
#include "Tools.h"
#include "NevoGame.h"

class MainWindow: public QMainWindow, public Renderer
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    void Render(const Rect &screenCoords, int textureId, const Rect &textureCoord);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    ~MainWindow();

private:
    NevoGame *game;
    QImage *buffer;
    QImage *texture;
    QTimer *timer;
};

#endif // MAINWINDOW_H
