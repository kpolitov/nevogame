#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    game(NULL),
    buffer(NULL),
    texture(NULL),
    timer(NULL)
{
    setupUi(this);

    QString fileName = QDir().absoluteFilePath("image.jpeg");
    texture = new QImage(fileName);

    if (texture->isNull())
    {
        return;
    }

    this->setFixedSize(texture->width(), texture->height());
    setWindowTitle("NevoGame");
    setAttribute(Qt::WA_QuitOnClose);
    currentRenderer = static_cast<Renderer*>(this);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100); // 10 FPS is enough

    buffer = new QImage(texture->width(), texture->height(), QImage::Format_RGB888);
    buffer->fill(qRgb(255, 255, 255));

    game = new NevoGame();
    game->Initialize();
    game->SetImageSize(texture->width(), texture->height());
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    if (buffer != NULL)
    {
        QPainter p(this);
        p.drawImage(0, 0, *buffer);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (game != NULL)
    {
        game->Click(event->x(), event->y());
    }
}

void MainWindow::Render(const Rect &screenCoords, int textureId, const Rect &textureCoord)
{
    if ((buffer != NULL) && (texture != NULL))
    {
        QPainter p(buffer);
        p.drawImage((int)screenCoords.left,
                    (int)screenCoords.top,
                    *texture,
                    (int)textureCoord.left,
                    (int)textureCoord.top,
                    (int)textureCoord.right-(int)textureCoord.left+1,
                    (int)textureCoord.bottom-(int)textureCoord.top+1);
    }
}

MainWindow::~MainWindow()
{
    if (timer != NULL)
    {
        timer->stop();
        delete timer;
        timer = NULL;
    }

    if (game != NULL)
    {
        delete game;
        game = NULL;
    }

    if (buffer != NULL)
    {
        delete buffer;
        buffer = NULL;
    }

    if (texture != NULL)
    {
        delete texture;
        texture = NULL;
    }
}
