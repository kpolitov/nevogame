#include "NevoGame.h"

NevoGame::NevoGame()
{
     m_Blocks = NULL;
     m_eState = NGS_NULL;
}

NevoGame::~NevoGame()
{
    m_eState = NGS_NULL;
    if (m_Blocks != NULL)
    {
        delete [] m_Blocks;
        m_Blocks = NULL;
    }
}

void NevoGame::Initialize()
{
    if (m_eState != NGS_NULL)
    {
        return;
    }

    if ((cColumns != 0) && (cRows != 0))
    {
        m_Blocks = new int[cRows*cColumns];
        for (int i=0; i<cRows*cColumns; m_Blocks[i++]=cRows*cColumns-i);
    }

    m_nWidth = 0;
    m_nHeight = 0;
    m_nSelectedBlock = -1;
    m_eState = NGS_INITIALIZED;
}

void NevoGame::Click(float x, float y)
{
    if (m_eState == NGS_RUNNING)
    {
        if ((m_nSelectedBlock = CoordsToBlockId((int)x, (int)y)) != -1)
        {
            m_eState = NGS_BLOCKSELECTED;
        };
    }
    else if (m_eState == NGS_BLOCKSELECTED)
    {
        int nSelectedBlock = CoordsToBlockId((int)x, (int)y);
        if (IsNeighbourBlock(m_nSelectedBlock, nSelectedBlock))
        {
            SwapBlocks(nSelectedBlock, m_nSelectedBlock);
            Render();

            if (CheckForWin())
            {
                m_eState = NGS_ENDED;
                return;
            }

        }
        m_nSelectedBlock = -1;
        m_eState = NGS_RUNNING;
    }
}

bool NevoGame::IsComplete() const
{
    return (m_eState == NGS_ENDED);
}

void NevoGame::Render() const
{
    Rect A, B;

    for (int i=0; i < cColumns*cRows; i++)
    {
        A = BlockIdToCoords(i);
        B = BlockIdToCoords(m_Blocks[i]);
        ::Render(A, cTextureId, B);
    }
}

void NevoGame::SetImageSize(int width, int height)
{
    if (m_eState == NGS_INITIALIZED)
    {
        m_nWidth = width;
        m_nHeight = height;
        m_eState = NGS_RUNNING;
        Render();
    }
}

bool NevoGame::IsNeighbourBlock(int blockId, int possibleNeighbourId)
{
    if ((blockId < 0) || (blockId >= cColumns*cRows) ||
       (possibleNeighbourId < 0) || (possibleNeighbourId >= cColumns*cRows))
    {
        return false;
    }

    Rect A(blockId % cColumns, blockId / cColumns, 0, 0);
    Rect B(possibleNeighbourId % cColumns, possibleNeighbourId / cColumns, 0, 0);

    if (A.left == B.left)
    {
        if ((B.top == A.top+1) || (B.top+1 == A.top))
        {
            return true;
        }
    }
    else if (A.top == B.top)
    {
        if ((B.left == A.left+1) || (B.left+1 == A.left))
        {
            return true;
        }
    }

    return false;
}

bool NevoGame::CheckForWin()
{
    for (int i=0; i < cColumns*cRows; i++)
    {
        if (m_Blocks[i] != i)
        {
            return false;
        }
    }

    return true;
}

void NevoGame::SwapBlocks(int nextId, int previousId)
{
    int nTempBlock = m_Blocks[nextId];
    m_Blocks[nextId] = m_Blocks[previousId];
    m_Blocks[previousId] = nTempBlock;
}


int NevoGame::CoordsToBlockId(int x, int y) const
{
    if ((x < m_nWidth) && (x >= 0) && (y < m_nHeight) && (y >= 0))
    {
        int nBlockWidth = m_nWidth/cColumns;
        int nBlockHeight = m_nHeight/cRows;
        return (y/nBlockHeight) * cColumns + x/nBlockWidth;
    }
    else
    {
        return -1;
    }
}

int NevoGame::CoordsToBlockId(Rect point) const
{
    return CoordsToBlockId((int)point.left, (int)point.top);
}

Rect NevoGame::BlockIdToCoords(int id) const
{
    Rect result;

    int nBlockWidth = m_nWidth/cColumns;
    int nBlockHeight = m_nHeight/cRows;
    result.left = nBlockWidth*(id % cColumns);
    result.top = nBlockHeight*(id / cColumns);
    result.right = result.left + nBlockWidth;
    result.bottom = result.top + nBlockHeight;

    return result;
}

void Render(const Rect& screenCoords, int textureId, const Rect& textureCoord)
{
    currentRenderer->Render(screenCoords, textureId, textureCoord);
}
