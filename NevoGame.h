#ifndef NEVOGAME_H
#define NEVOGAME_H

#include "Tools.h"

#include <cstddef>

/*  Интерфейс MiniGame спроектирован недостаточно успешно. Подразумевается работа
	с текстурами на стороне рендерера, вызываемого отдельно вызовом render(). При этом
	текстура (фоновая картинка), загружаясь отдельно от игрового объекта, не возвращает
	информацию о своем размере, необходимую для расчета игровой ситуации методом Click().
	Для обеспечения неизменности интерфейса MiniGame в класс-наследник был добавлен
	метод SetImageRect(). Из-за этого я не могу использовать полиморфизм при обращении
	из класса MainWindow. Также, запись virtual ~MiniGame() = 0; не имеет смысла. Я
	заменил ее на не-полностью виртуальный деструктор. */

class MiniGame
{
public:
    virtual ~MiniGame() {}
    virtual void Initialize() = 0;
    virtual void Click(float x, float y) = 0;
    virtual bool IsComplete() const = 0;
    virtual void Render() const = 0;

public:
    static const int cTextureId = 1; // Смысл этого поля не ясен.
    static const int cColumns = 5;
    static const int cRows = 5;
};

enum NevoGameState
{
    NGS_NULL,
    NGS_INITIALIZED,
    NGS_RUNNING,
    NGS_BLOCKSELECTED,
    NGS_ENDED
};

extern void Render(const Rect& screenCoords, int textureId, const Rect& textureCoord);

class NevoGame: public MiniGame
{
public:
    NevoGame();
    virtual ~NevoGame();
    virtual void Initialize();
    virtual void Click(float x, float y);
    virtual bool IsComplete() const;
    virtual void Render() const;
    void SetImageSize(int width, int height);

private:
    NevoGameState m_eState;
    int m_nWidth;
    int m_nHeight;
    int m_nSelectedBlock;
    int *m_Blocks;

    int m_nAnimBlockStart;
    int m_nAnimBlockFinish;
    float m_dAnimProgress;
    static const float m_dAnimSpeed = 0.001;

    bool CheckForWin();
    bool IsNeighbourBlock(int blockId, int possibleNeighbourId);
    void SwapBlocks(int nextId, int previousId);

    int CoordsToBlockId(int x, int y) const;
    int CoordsToBlockId(Rect point) const;
    Rect BlockIdToCoords(int id) const;
};

#endif // NEVOGAME_H
